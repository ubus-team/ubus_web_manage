export const state = () => ({
  auth: false
})

export const mutations = {
  setAuth(state, auth) {
    console.log('Yep set auth: ' + auth)
    state.auth = auth;
  },
}
export const getters = {
  isAuth: (state) => {
    return state.auth;
  },
}

export const actions = {
  login({ commit }) {
    commit('setAuth', true)
  },
  logout({ commit }) {
    commit('setAuth', false)
  },
  isAuth({ state }) {
    return state.auth;
  }
}
