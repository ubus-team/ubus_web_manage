
export const actions = {
  getNews ({ commit }, data) {
    return this.$axios.get('api/v1/advert/list').then(res => res.data)
  },
  getAllCar ({ commit }, data) {
    return this.$axios.get('api/location', data).then(res => res.data)
  },

  getAllPending ({ commit }, data) {
    return this.$axios.get('api/v1/bus/pending', data).then(res => res.data)
  },

  getAllStation ({ commit }, data) {
    return this.$axios.get('api/v1/station/list', data).then(res => res.data)
  },

  getAllLog ({ commit }, data) {
    return this.$axios.get('api/v1/log/get', {
      params: {
        limit: data ? data.limit : null,
        page: data ? data.page : null,
        unique: data ? data.unique : null
      }
    }).then(res => res.data)
  },

  editCar ({ commit }, data) {
    return this.$axios.post('api/v1/bus/edit', data).then(res => res.data)
  },

  editStation ({ commit }, data) {
    return this.$axios.post('api/v1/station/edit', data).then(res => res.data)
  },

  createCar ({ commit }, data) {
    return this.$axios.post('api/v1/bus/create', data).then(res => res.data)
  },

  createStation ({ commit }, data) {
    return this.$axios.post('api/v1/station/create', data).then(res => res.data)
  },

  deleteCar ({ commit }, data) {
    return this.$axios.delete('api/v1/bus/destroy', { data }).then(res => res.data)
  },

  deleteStation ({ commit }, data) {
    return this.$axios.delete('api/v1/station/destroy', { data }).then(res => res.data)
  },

  getADS ({ commit }, data) {
    return this.$axios.get('api/v1/advert/list', data).then(res => res.data)
  },
  uploadImage ({ commit }, data) {
    return this.$axios.post('api/upload/image', data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(res => res.data)
  },
  createADS ({ commit }, data) {
    return this.$axios.post('api/v1/advert/create', data).then(res => res.data)
  },

  editADS ({ commit }, data) {
    return this.$axios.post('api/v1/advert/edit', { data }).then(res => res.data)
  },

  deleteADS ({ commit }, data) {
    return this.$axios.delete('api/v1/advert/destroy', { data }).then(res => res.data)
  },

  getLogCSV ({ commit }, data) {
    return this.$axios.get('api/v1/log/get?csv=1', { responseType: 'blob', data }).then(res => res.data)
  }

}
