export default async function({ store, redirect, route }) {
  if(!(await store.dispatch('isAuth'))) {
    redirect('/login')
  }
}
