export default async function ({app, store, redirect}) {
  if ((await store.dispatch('isAuth'))) {
    return redirect('/bus');
  }
}
